import _ from "lodash"

const enable = true

let style_error   = 'background-color: rgba(255, 0, 0, .2); font-size: 1.2rem; color: red    '
let style_success = 'background-color: rgba(0, 255, 0, .2); font-size: 1.2rem; color: green  '
let style_default = 'background-color: rgba(0, 0 ,  0,  0); font-size: 1.2rem; color: blue   '
let style_send    = 'background-color: rgba(0, 0, 255, .1); font-size: 1.2rem; color: brown  '

Console.err     = (...data) => enable ? console.log.apply(console, ['%c 😢 ERROR: '  , style_error   , ...data]) : null
Console.success = (...data) => enable ? console.log.apply(console, ['%c 😎 SUCCESS: ', style_success , ...data]) : null
Console.send    = (...data) => enable ? console.log.apply(console, ['%c ✉ Send: '   , style_send    , ...data]) : null

export default function Console( ...data ) {
  enable ? console.log.apply(console, ['%c ℹ Info: ', style_default, data]) : null
}