const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");
const extractSass = new ExtractTextPlugin({
	filename: "app/dist/css/[name].css",
});

module.exports = {
	entry: [
		"babel-polyfill",
		"./app/src/app.js",
		"./app/src/**/*.sass",
	],
	output: {
		path: __dirname,
		filename: "./app/dist/app.js",
	},
	devServer: {
		port: 3000,
		historyApiFallback: true,
		compress: true,
		inline: true
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader:
					"babel-loader",
					options: {
						presets: ["react", "es2015", "stage-0"]
					}
				}
			},
			{
				test: /\.(scss||sass)$/,
				loader: extractSass.extract({
					use: [
						{
							loader: "css-loader", options: {
								url: false
							}
						},
						{
							loader: "postcss-loader", options: {
								plugins: function () {
									return [
										require('precss'),
										require('autoprefixer')
									];
								}
							}
						},
						"sass-loader",
					],
					fallback: "style-loader"
				})
			},
		]
	},
	watch: true,

	plugins: [
		extractSass
	]
}